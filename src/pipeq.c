/* PipeQ
 *
 * Copyright © 2021 Fabian Hügel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#define _GNU_SOURCE // asprintf
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <pipewire/context.h>
#include <pipewire/core.h>
#include <pipewire/extensions/metadata.h>
#include <pipewire/impl-module.h>
#include <pipewire/keys.h>
#include <pipewire/log.h>
#include <pipewire/main-loop.h>
#include <pipewire/node.h>
#include <pipewire/pipewire.h>
#include <pipewire/stream.h>
#include <spa/param/audio/format-utils.h>
#include <spa/utils/defs.h>
#include <spa/utils/json.h>
#include <spa/utils/result.h>

#include "array.h"
#include "plugin.h"

// TODO support different/dynamic sample rates?

#if TRACK_PORTS
struct port {
	struct pw_proxy *proxy;
	struct spa_hook proxy_listener;
	struct data *data;
	struct node *node;
	enum spa_direction direction;
	uint32_t port_id;
};
#endif

struct link {
	struct pw_proxy *proxy;
	struct spa_hook proxy_listener;
	struct data *data;
	struct node *from_node;
	struct node *to_node;
	uint32_t link_id;
};

struct node {
	struct pw_proxy *proxy;
	struct spa_hook proxy_listener;
	struct data *data;
	char *name;
	uint32_t node_id;
	array_t(struct link *) input_links;
	array_t(struct link *) output_links;
#if TRACK_PORTS
	array_t(struct port *) input_ports;
	array_t(struct port *) output_ports;
#endif
};

struct data {
	struct pw_main_loop *loop;
	struct pw_stream *sink_stream;
	struct spa_hook sink_listener;
	struct pw_stream *source_stream;
	struct spa_hook source_listener;
	struct node *source_node;
	struct pw_registry *registry;
	struct spa_hook registry_listener;
	struct pw_context *context;
	struct pw_core *core;
	struct spa_hook core_listener;
	struct spa_hook core_proxy_listener;
	struct pw_metadata *metadata;
	struct spa_hook metadata_listener;
	array_t(struct node *) nodes;
	array_t(struct link *) links;
#if TRACK_PORTS
	array_t(struct port *) ports;
#endif
	struct node *default_sink;

	int config_dir_wd;
	struct spa_source *inotify_source;
	char *config_name;
	pthread_spinlock_t plugins_lock;
	array_t(struct plugin *) plugins;
};

static const char *SINK_NODE_NAME = "PipeQ sink";
static const char *SOURCE_NODE_NAME = "PipeQ source";

static bool string_to_uint32(const char *str, uint32_t *result)
{
	char *endptr;
	unsigned long l = strtoul(str, &endptr, 10);
	if (*endptr != '\0' || (l == ULONG_MAX && errno == ERANGE)) {
		return false;
	}
	*result = (uint32_t)l;
	return true;
}

static void sink_stream_process(void *_data)
{
	// struct timespec start, end;
	// clock_gettime(CLOCK_MONOTONIC, &start);

	struct data *data = _data;

	struct pw_buffer *pw_buffer_in = pw_stream_dequeue_buffer(data->sink_stream);
	struct pw_buffer *pw_buffer_out = pw_stream_dequeue_buffer(data->source_stream);
	if (!pw_buffer_in || !pw_buffer_out) {
		goto done;
	}
	struct spa_buffer *inbuf = pw_buffer_in->buffer;
	struct spa_buffer *outbuf = pw_buffer_out->buffer;
	assert(inbuf->n_datas == 2);
	assert(outbuf->n_datas == 2);
	uint32_t size = inbuf->datas[0].chunk->size;
	assert(size == inbuf->datas[1].chunk->size);
	uint32_t n_samples = size / sizeof(float);

	const float *in[2] = { inbuf->datas[0].data, inbuf->datas[1].data };
	float *out[2] = { outbuf->datas[0].data, outbuf->datas[1].data };

	pthread_spin_lock(&data->plugins_lock);
	array_foreach_value(data->plugins, plugin) {
		plugin_run(plugin, in, out, n_samples);
		in[0] = out[0];
		in[1] = out[1];
	}
	pthread_spin_unlock(&data->plugins_lock);

	if (in[0] != out[0]) {
		memcpy(out[0], in[0], n_samples * sizeof(float));
	}
	if (in[1] != out[1]) {
		memcpy(out[1], in[1], n_samples * sizeof(float));
	}

	outbuf->datas[0].chunk->size = size;
	outbuf->datas[0].chunk->stride = sizeof(float);
	outbuf->datas[0].chunk->flags = 0;
	outbuf->datas[0].chunk->offset = 0;
	outbuf->datas[1].chunk->size = size;
	outbuf->datas[1].chunk->stride = sizeof(float);
	outbuf->datas[1].chunk->flags = 0;
	outbuf->datas[1].chunk->offset = 0;

done:
	if (pw_buffer_in) {
		pw_stream_queue_buffer(data->sink_stream, pw_buffer_in);
	}
	if (pw_buffer_out) {
		pw_stream_queue_buffer(data->source_stream, pw_buffer_out);
	}

	pw_stream_trigger_process(data->source_stream);

	// clock_gettime(CLOCK_MONOTONIC, &end);
	// double t = (end.tv_sec - start.tv_sec) * 1000.0 + (end.tv_nsec - start.tv_nsec) / 1000000.0;
	// fprintf(stderr, "%u samples %f ms (inbuf=%p, outbuf=%p)\n", n_samples, t, pw_buffer_in, pw_buffer_out);
}

static void state_changed(void *_data, enum pw_stream_state old,
			  enum pw_stream_state state, const char *error)
{
	struct data *data = _data;
	switch (state) {
	case PW_STREAM_STATE_PAUSED:
		pw_stream_flush(data->source_stream, false);
		pw_stream_flush(data->sink_stream, false);
		array_call_foreach_value(data->plugins, plugin_reset_state);
		break;
	default:
		break;
	}
}

static void destroy_plugin_list(struct plugin **plugins)
{
	array_call_foreach_value(plugins, plugin_destroy);
	array_free(plugins);
}

static void replace_plugins(struct data *data, struct plugin **new_plugins)
{
	if (data->plugins == new_plugins) {
		// should only happen when both are NULL
		return;
	}
	struct plugin **old_plugins = data->plugins;
	pthread_spin_lock(&data->plugins_lock);
	data->plugins = new_plugins;
	pthread_spin_unlock(&data->plugins_lock);
	destroy_plugin_list(old_plugins);
}

static bool load_config_file(struct data *data)
{
	const char *filename = data->config_name;
	if (!filename) {
		pw_log_warn("trying to load config file, but name is NULL");
		return false;
	}
	pw_log_info("loading config '%s'", filename);
	FILE *file = fopen(filename, "r");
	if (!file) {
		pw_log_warn("unable to open configuration file '%s': %s", filename, strerror(errno));
		return false;
	}
	bool failed = false;
	struct plugin **new_plugins = NULL;
	while (!failed && !feof(file)) {
		char dummy;
		if (fscanf(file, " #%*[^\n]%c", &dummy) == 1) {
			continue;
		}
		char *plugin_uri;
		if (fscanf(file, " [%m[^]\n]] %*[\n]", &plugin_uri) != 1) {
			if (feof(file)) {
				break;
			}
			pw_log_warn("failed to read configuration file '%s'", filename);
			failed = true;
			break;
		}
		struct plugin *plugin = plugin_create(plugin_uri, 48000);
		free(plugin_uri);
		if (!plugin) {
			failed = true;
			break;
		}
		array_add(new_plugins, plugin);
		struct key_value_pair *options = NULL;
		for (;;) {
			if (fscanf(file, " #%*[^\n]%c", &dummy) == 1) {
				continue;
			}
			struct key_value_pair option;
			fscanf(file, " ");
			int c = getc(file);
			if (c != EOF) {
				c = ungetc(c, file);
			}
			if (c == '[') {
				break;
			}
			if (c == EOF || fscanf(file, " %m[^: ] : %m[^\n] ", &option.key, &option.value) != 2) {
				if (feof(file)) {
					break;
				}
				pw_log_warn("failed to read configuration file '%s'", filename);
				failed = true;
				goto options_cleanup;
			}
			array_add(options, option);
		}

		if (!plugin_configure_ports(plugin, options, array_length(options))) {
			failed = true;
		}

	options_cleanup:
		array_foreach(options, it) {
			free(it->key);
			free(it->value);
		}
		array_free(options);
	}
	fclose(file);

	if (failed) {
		destroy_plugin_list(new_plugins);
		pw_log_warn("failed to apply new config, keeping old config");
		return false;
	}

	replace_plugins(data, new_plugins);

	return true;
}

static void on_inotify_event(void *_data, int inotify_fd, uint32_t mask)
{
	struct data *data = _data;
	if (!(mask & SPA_IO_IN)) {
		pw_log_warn("got unexpected inotify event");
		return;
	}

	char _Alignas(struct inotify_event) buf[4096];
	bool reload = false;
	bool unload = false;
	for (;;) {
		ssize_t len = read(inotify_fd, buf, sizeof(buf));
		if (len == -1 && errno != EAGAIN) {
			pw_log_error("inotify read failed: %s", strerror(errno));
			return;
		}

		if (len <= 0) {
			break;
		}

		const struct inotify_event *event;
		for (char *ptr = buf;
		     ptr < buf + len;
		     ptr += sizeof(struct inotify_event) + event->len) {
			event = (const struct inotify_event *)ptr;
			if (event->mask & IN_Q_OVERFLOW) {
				pw_log_warn("inotify event queue overflowed");
				continue;
			}
			if (event->wd != data->config_dir_wd) {
				continue;
			}
			if (event->mask & IN_IGNORED) {
				data->config_dir_wd = -1;
				continue;
			}
			if ((event->mask & IN_ISDIR) ||
			    !data->config_name ||
			    strcmp(event->name, data->config_name) != 0) {
				// ignore directories and all files that are not the current config
				continue;
			}
			if (event->mask & IN_MOVED_TO) {
				reload = true;
				unload = false;
			}
			if (event->mask & IN_CREATE) {
				reload = true;
				unload = false;
			}
			if (event->mask & IN_MOVED_FROM) {
				reload = false;
				unload = true;
			}
			if (event->mask & IN_DELETE) {
				reload = false;
				unload = true;
			}
			if (event->mask & IN_MODIFY) {
				if (unload) {
					// should not happen?
				} else {
					reload = true;
				}
			}
		}
	}

	assert(!(reload && unload));

	if (unload) {
		pw_log_info("config file '%s' was deleted, resetting config", data->config_name);
		replace_plugins(data, NULL);
		return;
	}

	if (reload) {
		load_config_file(data);
	}
}

static int compare_nodes_by_id(const void *_a, const void *_b)
{
	const struct node *a = *(const struct node **)_a;
	const struct node *b = *(const struct node **)_b;
	return (int)((int32_t)a->node_id - (int32_t)b->node_id);
}

static struct node **get_node_by_id(struct data *data, uint32_t id)
{
	struct node node = { .node_id = id };
	struct node *key = &node;
	return array_bsearch(data->nodes, &key, compare_nodes_by_id);
}

static void set_target_node(struct data *data, uint32_t node_id, const char *target_node_name)
{
	errno = 0;
	int res = pw_metadata_set_property(data->metadata, node_id, "target.node",
					   "Spa:String", target_node_name);
	(void)res;
	// fprintf(stderr, "pw_metadata_set_property: set target.node of %u to %s (res: %d, errno: %m)\n",
	// 	node_id, target_node_name ? target_node_name : "(NULL)", res);
}

static void maybe_redirect_link(struct data *data, uint32_t input_node_id, uint32_t output_node_id)
{
	uint32_t sink_id = pw_stream_get_node_id(data->sink_stream);
	uint32_t source_id = pw_stream_get_node_id(data->source_stream);
	uint32_t default_sink_id = data->default_sink ? data->default_sink->node_id : SPA_ID_INVALID;

	if (output_node_id != sink_id && output_node_id != source_id &&
	    input_node_id == default_sink_id) {
		set_target_node(data, output_node_id, SINK_NODE_NAME);
	}
}

static void set_default_sink(struct data *data, struct node *node)
{
	if (data->default_sink == node) {
		return;
	}

	free(data->config_name);
	data->config_name = NULL;

	data->default_sink = node;
	if (!node) {
		return;
	}

	char *filename = strdup(node->name);
	if (!filename) {
		pw_log_error("memory allocation failure");
		abort();
	}
	for (char *s = strchr(filename, '/'); s; s = strchr(s, '/')) {
		*s = '_';
	}
	data->config_name = filename;
	load_config_file(data);

	array_foreach_value(node->input_links, link) {
		uint32_t input_node_id = link->to_node ? link->to_node->node_id : SPA_ID_INVALID;
		uint32_t output_node_id = link->from_node ? link->from_node->node_id : SPA_ID_INVALID;
		assert(input_node_id == data->default_sink->node_id);
		maybe_redirect_link(data, input_node_id, output_node_id);
	}

	pw_log_info("switched to new sink: %s (%" PRIu32 ")", node->name, node->node_id);
}

static void on_source_links_changed(struct data *data)
{
	if (!data->source_node) {
		struct node **found = get_node_by_id(data, pw_stream_get_node_id(data->source_stream));
		assert(found);
		data->source_node = *found;
	}
	if (array_empty(data->source_node->output_links)) {
		set_default_sink(data, NULL);
		return;
	}
	struct node *default_sink = data->source_node->output_links[0]->to_node;
	set_default_sink(data, default_sink);
}

static void link_destroy(struct link *link)
{
	spa_hook_remove(&link->proxy_listener);
	pw_proxy_destroy(link->proxy);
	free(link);
}

static void link_delete_from_list(array_t(struct link *) *list, struct link *link)
{
	struct link **found = NULL;
	array_foreach(*list, it) {
		if (*it == link) {
			found = it;
		}
	}
	if (!found) {
		pw_log_warn("link not found in link list");
		return;
	}
	array_fast_delete(*list, array_index_of(*list, found));
	if (array_empty(*list)) {
		array_free(*list);
	}
}

static void on_link_removed(void *_link)
{
	struct link *link = _link;
	if (link->from_node) {
		if (link->from_node->node_id == pw_stream_get_node_id(link->data->source_stream)) {
			on_source_links_changed(link->data);
		}
		link_delete_from_list(&link->from_node->output_links, link);
	}
	if (link->to_node) {
		link_delete_from_list(&link->to_node->input_links, link);
	}
	link_delete_from_list(&link->data->links, link);
	link_destroy(link);
}

#if TRACK_PORTS
static void port_destroy(struct port *port)
{
	spa_hook_remove(&port->proxy_listener);
	pw_proxy_destroy(port->proxy);
	free(port);
}

static void port_delete_from_list(array_t(struct port *) *list, struct port *port)
{
	struct port **found = NULL;
	array_foreach(*list, it) {
		if (*it == port) {
			found = it;
		}
	}
	if (!found) {
		pw_log_warn("port not found in port list");
		return;
	}
	array_fast_delete(*list, array_index_of(*list, found));
	if (array_empty(*list)) {
		array_free(*list);
	}
}

static void on_port_removed(void *_port)
{
	struct port *port = _port;
	if (port->direction == SPA_DIRECTION_INPUT) {
		port_delete_from_list(&port->node->input_ports, port);
	} else {
		port_delete_from_list(&port->node->output_ports, port);
	}
	port_delete_from_list(&port->data->ports, port);
	port_destroy(port);
}
#endif

static void node_destroy(struct node *node)
{
	array_free(node->input_links);
	array_free(node->output_links);
#if TRACK_PORTS
	array_free(node->input_ports);
	array_free(node->output_ports);
#endif
	free(node->name);
	spa_hook_remove(&node->proxy_listener);
	pw_proxy_destroy(node->proxy);
	free(node);
}

static void on_node_removed(void *_node)
{
	struct node *node = _node;
	struct data *data = node->data;
	assert(node->node_id != pw_stream_get_node_id(data->sink_stream));
	assert(node->node_id != pw_stream_get_node_id(data->source_stream));

	if (node == data->default_sink) {
		// fprintf(stderr, "default sink removed\n");
		set_default_sink(data, NULL);
	}

	array_foreach_value(node->input_links, link) {
		assert(!link->to_node || link->to_node == node);
		link->to_node = NULL;
	}
	array_foreach_value(node->output_links, link) {
		assert(!link->from_node || link->from_node == node);
		link->from_node = NULL;
	}
#if TRACK_PORTS
	array_foreach_value(node->input_ports, port) {
		assert(!port->node || port->node == node);
		port->node = NULL;
	}
	array_foreach_value(node->output_ports, port) {
		assert(!port->node || port->node == node);
		port->node = NULL;
	}
#endif

	struct node **found = get_node_by_id(data, node->node_id);
	assert(found && *found == node);
	array_ordered_delete(data->nodes, array_index_of(data->nodes, found));
	// fprintf(stderr, "node removed: %s (id: %u)\n", node->name, node->node_id);
	node_destroy(node);
}

static void registry_event_global_node(struct data *data, uint32_t node_id, uint32_t version,
				       const struct spa_dict *props)
{
	assert(node_id != SPA_ID_INVALID);
	struct node **found = get_node_by_id(data, node_id);
	if (found) {
		pw_log_warn("node %u already registered", node_id);
#ifndef NDEBUG
		struct node *node = *found;
		assert(node->node_id == node_id);
		const char *node_name = spa_dict_lookup(props, PW_KEY_NODE_NAME);
		assert(node_name);
		assert(strcmp(node->name, node_name) == 0);
#endif
		return;
	}

	const char *node_name = spa_dict_lookup(props, PW_KEY_NODE_NAME);
	if (!node_name) {
		pw_log_warn("node has no name");
		return;
	}

	struct pw_proxy *proxy = pw_registry_bind(data->registry, node_id, PW_TYPE_INTERFACE_Node, version, 0);
	if (!proxy) {
		pw_log_warn("unable to bind node");
		return;
	}

	struct node *node = calloc(1, sizeof(*node));
	if (!node) {
		pw_log_error("memory allocation failure");
		abort();
	}
	node->node_id = node_id;
	node->data = data;
	node->proxy = proxy;
	node->name = strdup(node_name);
	if (!node->name) {
		pw_log_error("memory allocation failure");
		abort();
	}
	array_insert_sorted(data->nodes, node, compare_nodes_by_id);
	static const struct pw_proxy_events node_proxy_events = {
		PW_VERSION_PROXY_EVENTS,
		.removed = on_node_removed,
	};
	pw_proxy_add_listener(proxy, &node->proxy_listener,
			      &node_proxy_events, node);

}

#if TRACK_PORTS
static void registry_event_global_port(struct data *data, uint32_t port_id, uint32_t version,
				       const struct spa_dict *props)
{
	array_foreach_value(data->ports, it) {
		if (it->port_id == port_id) {
			return;
		}
	}

	const char *node_id_str = spa_dict_lookup(props, PW_KEY_NODE_ID);
	if (!node_id_str) {
		pw_log_warn("port %" PRIu32 " has no node id", port_id);
		return;
	}
	uint32_t node_id;
	if (!string_to_uint32(node_id_str, &node_id)) {
		pw_log_warn("cannot parse node id of port %" PRIu32, port_id);
		return;
	}
	struct node **found = get_node_by_id(data, node_id);
	if (!found) {
		pw_log_warn("node of port %" PRIu32 " not found in node list", port_id);
		return;
	}
	struct node *node = *found;
	const char *direction_str = spa_dict_lookup(props, PW_KEY_PORT_DIRECTION);
	if (!direction_str) {
		pw_log_warn("port %" PRIu32 " has no direction", port_id);
		return;
	}
	enum spa_direction direction;
	if (strcmp(direction_str, "in") == 0) {
		direction = SPA_DIRECTION_INPUT;
	} else if (strcmp(direction_str, "out") == 0) {
		direction = SPA_DIRECTION_OUTPUT;
	} else {
		pw_log_warn("port %" PRIu32 " has unknown direction", port_id);
		return;
	}

	struct pw_proxy *proxy = pw_registry_bind(data->registry, port_id, PW_TYPE_INTERFACE_Port, version, 0);
	if (!proxy) {
		pw_log_warn("unable to bind port %" PRIu32, port_id);
		return;
	}

	struct port *port = calloc(1, sizeof(*port));
	if (!port) {
		pw_log_error("memory allocation failure");
		abort();
	}
	port->proxy = proxy;
	port->data = data;
	port->port_id = port_id;
	port->direction = direction;
	port->node = node;
	array_add(data->ports, port);
	if (direction == SPA_DIRECTION_INPUT) {
		array_add(node->input_ports, port);
	} else {
		array_add(node->output_ports, port);
	}
	static const struct pw_proxy_events port_proxy_events = {
		PW_VERSION_PROXY_EVENTS,
		.removed = on_port_removed,
	};
	pw_proxy_add_listener(proxy, &port->proxy_listener,
			      &port_proxy_events, port);
}
#endif

static void registry_event_global_link(struct data *data, uint32_t link_id, uint32_t version,
				       const struct spa_dict *props)
{
	const char *input_node_id_str = spa_dict_lookup(props, PW_KEY_LINK_INPUT_NODE);
	const char *output_node_id_str = spa_dict_lookup(props, PW_KEY_LINK_OUTPUT_NODE);
	uint32_t input_node_id, output_node_id;
	if (!string_to_uint32(input_node_id_str, &input_node_id) ||
	    !string_to_uint32(output_node_id_str, &output_node_id)) {
		pw_log_warn("cannot parse input/output node id of link %" PRIu32, link_id);
		return;
	}

	maybe_redirect_link(data, input_node_id, output_node_id);

	array_foreach_value(data->links, it) {
		if (it->link_id == link_id) {
			return;
		}
	}

	struct node **output_node_pointer = get_node_by_id(data, output_node_id);
	if (!output_node_pointer) {
		pw_log_warn("output node of new link not found in node list");
		return;
	}
	struct node *output_node = *output_node_pointer;

	struct node **input_node_pointer = get_node_by_id(data, input_node_id);
	if (!input_node_pointer) {
		pw_log_warn("input node of new link not found in node list");
		return;
	}
	struct node *input_node = *input_node_pointer;

	struct pw_proxy *proxy = pw_registry_bind(data->registry, link_id, PW_TYPE_INTERFACE_Link, version, 0);
	if (!proxy) {
		pw_log_warn("unable to bind link");
		return;
	}

	struct link *link = calloc(1, sizeof(*link));
	if (!link) {
		pw_log_error("memory allocation failure");
		abort();
	}
	link->proxy = proxy;
	link->data = data;
	link->from_node = output_node;
	link->to_node = input_node;
	link->link_id = link_id;
	array_add(output_node->output_links, link);
	array_add(input_node->input_links, link);
	array_add(data->links, link);

	if (output_node->node_id == pw_stream_get_node_id(link->data->source_stream)) {
		on_source_links_changed(data);
	}

	static const struct pw_proxy_events link_proxy_events = {
		PW_VERSION_PROXY_EVENTS,
		.removed = on_link_removed,
	};
	pw_proxy_add_listener(proxy, &link->proxy_listener,
			      &link_proxy_events, link);
}

static void registry_event_global_metadata(struct data *data, uint32_t id, uint32_t version,
					   const struct spa_dict *props)
{
	const char* name = spa_dict_lookup(props, PW_KEY_METADATA_NAME);
	if (name && strcmp(name, "default") == 0) {
		if (data->metadata) {
			spa_hook_remove(&data->metadata_listener);
			pw_proxy_destroy((struct pw_proxy *)data->metadata);
		}
		data->metadata = pw_registry_bind(data->registry, id, PW_TYPE_INTERFACE_Metadata, version, 0);
	}
}

static void __attribute__((unused))
debug_print_registered_object(uint32_t id, uint32_t permissions, const char *type,
			      uint32_t version, const struct spa_dict *props)
{
	if (!type) {
		type = "???";
	}
	fprintf(stderr, "Type: %s, id: %" PRIu32 ", version: %" PRIu32 ", perm: %" PRIu32 "\n",
		type, id, version, permissions);
	if (!props) {
		fprintf(stderr, "\tNULL");
		return;
	}
	const struct spa_dict_item *item;
	spa_dict_for_each(item, props) {
		fprintf(stderr, "\t%s: %s\n", item->key, item->value);
	}
}

static void registry_event_global(void *_data, uint32_t id,
				  uint32_t permissions, const char *type, uint32_t version,
				  const struct spa_dict *props)
{
	struct data *data = _data;

	// debug_print_registered_object(id, permissions, type, version, props);

	if (!type) {
		return;
	}

	if (strcmp(type, PW_TYPE_INTERFACE_Node) == 0) {
		registry_event_global_node(data, id, version, props);
	}

	if (strcmp(type, PW_TYPE_INTERFACE_Link) == 0) {
		registry_event_global_link(data, id, version, props);
	}

#if TRACK_PORTS
	if (strcmp(type, PW_TYPE_INTERFACE_Port) == 0) {
		registry_event_global_port(data, id, version, props);
	}
#endif

	if (strcmp(type, PW_TYPE_INTERFACE_Metadata) == 0) {
		registry_event_global_metadata(data, id, version, props);
	}
}

static int roundtrip_pending, roundtrip_done;

static void core_event_done(void *_data, uint32_t id, int seq) {
	struct data *data = _data;
	if (id == PW_ID_CORE && seq == roundtrip_pending) {
		roundtrip_done = 1;
		pw_main_loop_quit(data->loop);
	}
}

static void core_destroy(void *_data)
{
	struct data *data = _data;
	spa_hook_remove(&data->core_listener);
	spa_hook_remove(&data->core_proxy_listener);
	data->core = NULL;
	pw_main_loop_quit(data->loop);
}

static int exit_status;

static void core_error(void *_data, uint32_t id, int seq, int res, const char *message)
{
	struct data *data = _data;

	pw_log_error("error id:%u seq:%d res:%d (%s): %s",
		     id, seq, res, spa_strerror(res), message);

	if (id == PW_ID_CORE && res == -EPIPE) {
		exit_status = 1;
		pw_main_loop_quit(data->loop);
	}
}

static int roundtrip(struct pw_core *core, struct pw_main_loop *loop)
{
	roundtrip_done = 0;
        roundtrip_pending = pw_core_sync(core, PW_ID_CORE, 0);
        while (!roundtrip_done) {
                pw_main_loop_run(loop);
        }
        return 0;
}

static void do_quit(void *userdata, int signal_number)
{
	struct data *data = userdata;
	pw_main_loop_quit(data->loop);
}

int main(int argc, char **argv)
{
	pw_init(&argc, &argv);

	char *config_dir = NULL;
	if (argc >= 2) {
		config_dir = strdup(argv[1]);
	} else {
		const char *config_home = getenv("XDG_CONFIG_HOME");
		if (config_home) {
			asprintf(&config_dir, "%s/pipeq", config_home);
		} else {
			const char *home = getenv("HOME");
			if (!home) {
				pw_log_error("getenv(\"HOME\") failed");
				exit_status = 1;
				goto exit_deinit;
			}
			asprintf(&config_dir, "%s/.config/pipeq", home);
		}
	}
	if (!config_dir) {
		pw_log_error("memory allocation failure");
		exit_status = 1;
		goto exit_deinit;
	}

	if (chdir(config_dir) == -1) {
		pw_log_error("unable to change working directory to config directory ('%s'): %s", config_dir,
			     strerror(errno));
		free(config_dir);
		exit_status = 1;
		goto exit_deinit;
	}
	free(config_dir);

	struct data *data = calloc(1, sizeof(*data));
	if (!data) {
		pw_log_error("memory allocation failure");
		exit_status = 1;
		goto exit_deinit;
	}
	data->config_dir_wd = -1;
	pthread_spin_init(&data->plugins_lock, PTHREAD_PROCESS_PRIVATE);

	if (!plugins_init()) {
		exit_status = 1;
		goto exit;
	}

	data->loop = pw_main_loop_new(NULL);
	if (!data->loop) {
		pw_log_error("unable to create main loop");
		exit_status = 1;
		goto exit;
	}

	struct pw_loop *loop = pw_main_loop_get_loop(data->loop);
	pw_loop_add_signal(loop, SIGINT, do_quit, data);
	pw_loop_add_signal(loop, SIGTERM, do_quit, data);

	int inotify_fd = inotify_init1(IN_CLOEXEC | IN_NONBLOCK);
	if (inotify_fd == -1) {
		pw_log_error("unable to create inotify instance");
		exit_status = 1;
		goto exit;
	}
	data->config_dir_wd = inotify_add_watch(inotify_fd, ".",
						IN_CREATE | IN_MOVED_TO | IN_DELETE | IN_MOVED_FROM |
						IN_MODIFY | IN_ONLYDIR | IN_EXCL_UNLINK);
	if (data->config_dir_wd == -1) {
		pw_log_warn("unable to add inotify watch: %s", strerror(errno));
		close(inotify_fd);
	} else {
		data->inotify_source = pw_loop_add_io(loop, inotify_fd, SPA_IO_IN, true, on_inotify_event, data);
		if (!data->inotify_source) {
			pw_log_error("unable to create inotify source");
			exit_status = 1;
			goto exit;
		}
	}

	data->context = pw_context_new(loop,
				       pw_properties_new(PW_KEY_MEDIA_TYPE, "Audio",
							 PW_KEY_MEDIA_CATEGORY, "Manager",
							 PW_KEY_MEDIA_ROLE, "Music",
							 PW_KEY_CONFIG_NAME, "client-rt.conf",
							 NULL),
				       0);
	if (!data->context) {
		pw_log_error("unable to create context");
		exit_status = 1;
		goto exit;
	}
	data->core = pw_context_connect(data->context, NULL, 0);
	if (!data->core) {
		pw_log_error("unable to connect");
		exit_status = 1;
		goto exit;
	}
	static const struct pw_proxy_events core_proxy_events = {
		PW_VERSION_PROXY_EVENTS,
		.destroy = core_destroy,
	};
	pw_proxy_add_listener((struct pw_proxy*)data->core, &data->core_proxy_listener,
			      &core_proxy_events, data);
	static const struct pw_core_events core_events = {
                PW_VERSION_CORE_EVENTS,
                .done = core_event_done,
		.error = core_error,
        };
	pw_core_add_listener(data->core, &data->core_listener, &core_events, data);

	struct pw_properties *sink_props = pw_properties_new(PW_KEY_MEDIA_TYPE, "Audio",
							     PW_KEY_MEDIA_CLASS, "Audio/Sink",
							     PW_KEY_NODE_NAME, SINK_NODE_NAME,
							     PW_KEY_NODE_GROUP, "pipeq",
							     // PW_KEY_NODE_LINK_GROUP, "pipeq",
							     PW_KEY_NODE_AUTOCONNECT, "false",
							     PW_KEY_NODE_VIRTUAL, "true",
							     PW_KEY_NODE_MAX_LATENCY, "2048/48000",
							     PW_KEY_OBJECT_LINGER, "false",
							     PW_KEY_AUDIO_RATE, "48000",
							     PW_KEY_AUDIO_CHANNELS, "2",
							     SPA_KEY_AUDIO_POSITION, "FL,FR",
							     NULL);
	data->sink_stream = pw_stream_new(data->core, SINK_NODE_NAME, sink_props);
	if (!data->sink_stream) {
		pw_log_error("unable to create sink");
		exit_status = 1;
		goto exit;
	}
	struct pw_stream_events sink_events = {
		PW_VERSION_STREAM_EVENTS,
		.process = sink_stream_process,
		.state_changed = state_changed,
	};
	pw_stream_add_listener(data->sink_stream, &data->sink_listener, &sink_events, data);

	struct pw_properties *source_props = pw_properties_new(PW_KEY_MEDIA_TYPE, "Audio",
							       PW_KEY_NODE_NAME, SOURCE_NODE_NAME,
							       PW_KEY_NODE_GROUP, "pipeq",
							       // PW_KEY_NODE_LINK_GROUP, "pipeq",
							       PW_KEY_NODE_PASSIVE, "true",
							       // PW_KEY_NODE_PAUSE_ON_IDLE, "true",
							       PW_KEY_NODE_VIRTUAL, "true",
							       PW_KEY_NODE_MAX_LATENCY, "2048/48000",
							       PW_KEY_OBJECT_LINGER, "false",
							       PW_KEY_AUDIO_RATE, "48000",
							       PW_KEY_AUDIO_CHANNELS, "2",
							       SPA_KEY_AUDIO_POSITION, "FL,FR",
							       NULL);
	data->source_stream = pw_stream_new(data->core, SOURCE_NODE_NAME, source_props);
	if (!data->source_stream) {
		pw_log_error("unable to create source");
		exit_status = 1;
		goto exit;
	}
	struct pw_stream_events source_events = {
		PW_VERSION_STREAM_EVENTS,
		.state_changed = state_changed,
	};
	pw_stream_add_listener(data->source_stream, &data->source_listener, &source_events, data);

	const struct spa_pod *params[1];
	uint8_t buffer[1024];
	struct spa_pod_builder builder = SPA_POD_BUILDER_INIT(buffer, sizeof(buffer));
	params[0] = spa_format_audio_raw_build(&builder, SPA_PARAM_EnumFormat,
					       &SPA_AUDIO_INFO_RAW_INIT(.format = SPA_AUDIO_FORMAT_F32P,
									.channels = 2,
									.rate = 48000,
									.position[0] = SPA_AUDIO_CHANNEL_FL,
									.position[1] = SPA_AUDIO_CHANNEL_FR));

	if (pw_stream_connect(data->sink_stream, PW_DIRECTION_INPUT, PW_ID_ANY,
			      PW_STREAM_FLAG_RT_PROCESS | PW_STREAM_FLAG_MAP_BUFFERS, params, 1) < 0) {
		pw_log_error("unable to connect sink");
		exit_status = 1;
		goto exit;
	}

	if (pw_stream_connect(data->source_stream, PW_DIRECTION_OUTPUT, PW_ID_ANY,
			      PW_STREAM_FLAG_RT_PROCESS | PW_STREAM_FLAG_MAP_BUFFERS |
			      PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_TRIGGER, params, 1) < 0) {
		pw_log_error("unable to connect source");
		exit_status = 1;
		goto exit;
	}

	unsigned int num_roundtrips = 0;
	while (pw_stream_get_node_id(data->sink_stream) == SPA_ID_INVALID ||
	       pw_stream_get_node_id(data->source_stream) == SPA_ID_INVALID) {
		if (num_roundtrips == 10) {
			pw_log_error("initialization failed");
			exit_status = 1;
			goto exit;
		}
		usleep(num_roundtrips * 100000);
		roundtrip(data->core, data->loop);
		num_roundtrips++;
	}

	data->registry = pw_core_get_registry(data->core, PW_VERSION_REGISTRY, 0);
	static const struct pw_registry_events registry_events = {
		PW_VERSION_REGISTRY_EVENTS,
		.global = registry_event_global,
	};
	pw_registry_add_listener(data->registry, &data->registry_listener, &registry_events, data);

	pw_main_loop_run(data->loop);

exit:
	array_call_foreach_value(data->nodes, node_destroy);
	array_free(data->nodes);
#if TRACK_PORTS
	array_foreach_value(data->ports, port) {
		port_destroy(port);
	}
	array_free(data->ports);
#endif
	array_call_foreach_value(data->links, link_destroy);
	array_free(data->links);
	if (data->inotify_source) {
		pw_loop_destroy_source(pw_main_loop_get_loop(data->loop), data->inotify_source);
	}
	if (data->metadata) {
		pw_proxy_destroy((struct pw_proxy *)data->metadata);
	}
	if (data->registry) {
		spa_hook_remove(&data->registry_listener);
		pw_proxy_destroy((struct pw_proxy *)data->registry);
	}
	if (data->sink_stream) {
		spa_hook_remove(&data->sink_listener);
		pw_stream_destroy(data->sink_stream);
	}
	if (data->source_stream) {
		spa_hook_remove(&data->source_listener);
		pw_stream_destroy(data->source_stream);
	}
	if (data->core) {
		pw_core_disconnect(data->core);
	}
	if (data->context) {
		pw_context_destroy(data->context);
	}
	if (data->loop) {
		pw_main_loop_destroy(data->loop);
	}

	free(data->config_name);

	pthread_spin_destroy(&data->plugins_lock);
	destroy_plugin_list(data->plugins);
	plugins_deinit();

	free(data);

exit_deinit:
	pw_deinit();
	return exit_status;
}
