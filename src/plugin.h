/* PipeQ
 *
 * Copyright © 2021 Fabian Hügel
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef __EQUALIZER_INCLUDE__
#define __EQUALIZER_INCLUDE__

#include <stdbool.h>
#include <stdint.h>

struct plugin;

struct key_value_pair {
	char *key;
	char *value;
};

struct plugin *plugin_create(const char *uri, double sample_rate);
void plugin_reset_ports(struct plugin *plugin);
void plugin_reset_state(struct plugin *plugin);
bool plugin_configure_ports(struct plugin *plugin, const struct key_value_pair *options, size_t n_options);
void plugin_run(struct plugin *plugin, const float *in[2], float *out[2], uint32_t n_samples);
void plugin_destroy(struct plugin *plugin);

bool plugins_init(void);
void plugins_deinit(void);

#endif
